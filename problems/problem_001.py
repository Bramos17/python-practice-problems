# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def minimum_value(value1, value2):  # looking for min value
    value1 = 30
    value2 = 50
    if value1 < value2:     # if value1 less then value2
        return value1       # we will get Value1
    else:                   # if not (or Else)
        return value2       # return value2


print(minimum_value(30, 50))
