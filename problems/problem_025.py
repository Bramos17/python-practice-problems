# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    if len(values) == 0:
        return None
    sum = 0
    for item in values:
        sum = sum + item
    return sum


number = [4, 5, 9, 3]
print(calculate_sum(number))

# step 1(l8)-  if there are no items in the list of values use len()==0
# step 2(l9)-  retruns NONE as len()== 0
# step 3(l10)- sum # of values list is none so sum == 0
