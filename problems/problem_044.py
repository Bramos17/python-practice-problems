# Complete the translate function which
# accepts two parameters, a list of keys and a dictionary.
# It returns a new list that contains the values of the corresponding
# keys in the dictionary.
# If the key does not exist, then the list should contain a None for that key.
# Examples:
#   * keys:       ["name", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     ["Noor", 29]
#   * keys:       ["eye color", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [None, 29]
#   * keys:       ["age", "age", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [29, 29, 29]
#
# Remember that a dictionary has the ".get" method on it.

def translate(key_list, dictionary):
    result = []
    for key in key_list:
        result.append(dictionary.get(key))
    return result


print(translate(["name", "age"], {"name": "Noor", "age": 29}))
print(translate(["eye color", "age"], {"name": "Noor", "age": 29}))
print(translate(["age", "age", "age"], {"name": "Noor", "age": 29}))

# step 1(20)-  define the function, and (set paramiters)
# step 2(21)- a new list that contains the values of the corresponding
#               keys in the dictionary.
# step 3(22)- theres a lot that happens here, here is the first step
#      * we run the fuction (for key in Key_list)
#      * then we use the .git methoid to obtain the key in the dictionary
#      * dictionary.git(key)-- if that key is not there it returns as NONE
#      *!now we also have to append"rename" the key to match the key in
#      *!coresponding dictionary  per directions and we want the result
# step 4(23)-  now we retun the result
# step 5(26)-finaly to check the code we use our examples as listed above
#              and print
