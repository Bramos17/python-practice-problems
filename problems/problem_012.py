# Complete the fizzbuzz function to return
# * The word "fizzbuzz" if number is evenly divisible by
#   by both 3 and 5
# * The word "fizz" if number is evenly divisible by only
#   3
# * The word "buzz" if number is evenly divisible by only
#   5
# * The number if it is not evenly divisible by 3 nor 5
#
# Try to combine what you have done in the last two problems
# from memory.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def fizzbuzz(number):
    if number % 3 == 0 and number % 5 == 0:  # determining if number can be
        # % by 3 & 5
        return "fizzbuzz"
    elif number % 3 == 0:           # determin if number can be % olny 3
        return "fizz"
    elif number % 5 == 0:           # determin if number can be % olny 5
        return "buzz"
    else:
        return number               # Or else if none of the above apply.


print(fizzbuzz(30))
