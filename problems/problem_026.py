# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(values):
    average = sum(values) / len(values)
    int(average)
    if average >= (90) <= (100):
        return "A"
    elif average >= (80) <= (90):
        return "B"
    elif average >= (70) <= (80):
        return "C"
    elif average >= (60) <= (70):
        return "D"
    else:
        return "F"


values = [89]
print(calculate_grade(values))

# step 1(l6)-     detrimine a value and make it into intiger b4 math
# step 2(l8-l27)- determine the paramiters
# step 3(l30)-    determine the grade
# steop 4(l31)-   print function
