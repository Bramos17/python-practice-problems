# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    if len(values) == 1:         # if there is only one value(parameter)
        return None              # return None
    if len(values) == 0:         # if there are no values(parameter)
        return None              # return None
# after defining the paramiteers create the function

    largest = max(values[0], values[1])         # find the largest value
    Second_largest = min(values[0], values[1])  # find the second largest
    for val in values:                          # for each value in list
        if val > Second_largest:                # if the value is greater
            Second_largest = largest            # update the largest value
            largest = val                       # larget value = val
        elif val > Second_largest:              # if the value is greater
            Second_largest = val                # update the second largest
    return Second_largest                       # return the second largest


print(find_second_largest([1, 2, 3, 4, 5, 7, 8, 9, 10]))
# output: 9

# (paramater 1)
print(find_second_largest([1]))

# (paramater 2)
print(find_second_largest([]))
# output: none
