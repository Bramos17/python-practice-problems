# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def reverse_string(new_string):      # def (built in function)(value)
    letters_list = list(new_string)  # define list and mk list of value
    letters_list.reverse()           # use .reverse built in fucntion
    return ''.join(letters_list)     # ("") & .join reversed lst & new lst


def is_palindrome(orig_string):    # def the function and add new_string
    return orig_string == reverse_string(orig_string)
# orig string is == reversed_string(value)


print(is_palindrome("racecar"))
# print example "racecar"--output--True
# b/c it is the same printed fwd & back
