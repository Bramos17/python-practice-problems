# Complete the sum_of_squares function which accepts
# a list of numerical values and returns the sum of
# each item squared
#
# If the list of values is empty, the function should
# return None
#
# Examples:
#   * [] returns None
#   * [1, 2, 3] returns 1*1+2*2+3*3=14
#   * [-1, 0, 1] returns (-1)*(-1)+0*0+1*1=2
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def sum_of_squares(values):             # define the function(parameter)
    if len(values) == 0:                # if the list is empty
        return None                     # return None
    sum_of_squares = 0                  # initialize the sum of squares
    for value in values:                # for each value in the list
        sum_of_squares += value ** 2    # add the value to the sum of squares
    return sum_of_squares               # return the sum of squares


print(sum_of_squares([]))
# output: None
print(sum_of_squares([1, 2, 3]))
# output: 14
print(sum_of_squares([-1, 0, 1]))
# output: 2
