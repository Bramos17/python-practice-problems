# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
# l5- create a list = gear
def gear_for_day(is_workday, is_sunny):
    gear = []
    if is_workday and not is_sunny:
        gear.append("umbrella")

    if is_workday:
        gear.append("laptop")

    if is_sunny:
        gear.append("sufrboard")
    return (gear)


print(gear_for_day("is_workday", "is_sunny"))


# !Paramaters:
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"--line 7
#   * If it is not a workday, the list needs to contain
#     "surfboard"
#   * If it is a workday, the list needs to contain "laptop"
#    line-8 it is a workfday, so append gear to add laptop.
#    No detrm if sunny or not
