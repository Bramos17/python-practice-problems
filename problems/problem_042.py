# Complete the pairwise_add function which accepts two lists
# of the same size. It creates a new list and populates it
# with the sum of corresponding entries in the two lists.
#
# Examples:
#   * list1:  [1, 2, 3, 4]
#     list2:  [4, 5, 6, 7]
#     result: [5, 7, 9, 11]
#   * list1:  [100, 200, 300]
#     list2:  [ 10,   1, 180]
#     result: [110, 201, 480]
#
# Look up the zip function to help you with this problem.


def pairwise_add(list1, list2):
    results = []
    zipped_list = zip(list1, list2)
# print(list(zipped_list)) ->this is a test and will not allow fuction to print
    for value1, value2, in zipped_list:
        results.append(value1 + value2)
    return results


print(pairwise_add([1, 2, 3, 4], [4, 5, 6, 7]))

# step 1(l16)-  define pairwise_add by its parimiters of (list1, and list2)
# step 2(l17)-  define results as a list
# step 3(l18)-  zip funcion will combine 2 files = same size into 1 file
# step 4(l20)-  print out the new created list(zipped_list)
# step 5(l21)-  for values1, and value2 in zipped_list
# step 6(l22)-  append Value1 and Value2
# step 7(l23)-  return results
# step *(l26)-  print fuction to test
