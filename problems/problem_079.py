# Write a class that meets these requirements.
#
# Name:       ReceiptItem
#
# Required state:
#    * quantity, the amount of the item bought
#    * price, the amount each one of the things cost
#
# Behavior:
#    * get_total()          # Returns the quantity * price
#
# Example:
#    item = ReceiptItem(10, 3.45)
#
#    print(item.get_total())    # Prints 34.5


class ReceiptItem:                           # class as made up from above
     def __init__(self, quantity, price):    # allways defining __init__
        self.quantity = quantity             # always passing through self
        self.price = price

     def get_total(self):                    # Defineing the behavior
          return self.quantity * self.price    # 24-return the total behavior


item = ReceiptItem(10, 3.45)                 # 26-As provided above example

print(item.get_total())                      # 27-example Prints 34.5
