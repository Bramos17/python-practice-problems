# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# # Pseudocode is available for you

def calculate_average(values):
    if len(values) == 0:
        return None
    sum = 0
    for item in values:
        sum = sum + item
    return sum/len(values)


print(calculate_average([1, 2, 3, 4, 5]))


# step 1(l8)-  if there are no items in the list of values use len()==0
# step 2(l9)-  retruns NONE as len()== 0
# step 3(l10)- sum # of values list is none so sum == 0
# step 4(l11)- for the items in values add each item to sum of values
# step 5(l12)- list(sum= sum + items)
# step 6(l13)- return items sum and devide by num of items
# step 7(l14)- print function
