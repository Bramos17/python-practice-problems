# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if len(values) == 0:
        return None
    maxvalue = values[0]
    for value in values:
        if value > maxvalue:
            maxvalue = value
    return maxvalue


values = [4, 5, 6, 8, 9, 152]
print(max_in_list(values))

# step 1(7)-   define problem we are looking to find the maximn value in a list
# step 2(8)-   define results as a list
# step 3(9)-   zip funcion will combine 2 files = same size into 1 file
# step 4(10)-  print out the new created list(zipped_list)
# step 5(11)-  for values1, and value2 in zipped_list
# step 6(12)-  append Value1 and Value2
# step 7(13)-  return results
# step 8(14)-  print fuction to test
