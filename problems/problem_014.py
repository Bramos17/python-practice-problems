# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_make_pasta(ingredients):
    if ("flour" in ingredients and "eggs" in ingredients and "oil" in
            ingredients):
        return True
    else:
        return False


print(can_make_pasta("bread"))


# 13- def= define, Can_make_pasta = function, (ingredients)= paramiter
# 14- If statement defining the paramiters that need to be in the function
# 15- if those paramiters are met(returns with true)
# 16- op else statement- saying if the imputed paramiter is not as defined in
# 17-      if statement then it will return False
# 18- print the (function, "with the Input parimater")
