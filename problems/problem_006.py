# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_skydive(age, has_consent_form):
    if age >= 18 and has_consent_form:  # if 18 and over and has consent form
        return True                     # ^ they can go skydiving
    if age <= 17 and has_consent_form:  # if 17 and under and has consent form
        return "True"                     # ^ they can go skydiving
    else:                               # if nothing above is true than
        return "False"                    # they can not go sky diving.
