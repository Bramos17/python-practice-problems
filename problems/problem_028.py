# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

def remove_duplicate_letters(s):
    if len(s) == 0:
        return ''
    new_string = ''
    for char in s:
        if char not in new_string:
            new_string += char
    return new_string


print(remove_duplicate_letters("abc"))
print(remove_duplicate_letters("abcabc"))
print(remove_duplicate_letters("abccba"))
print(remove_duplicate_letters("abccbad"))
